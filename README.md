# Vagrant & Docker Workshop
2022-02-10

~45min presentation, ~45min praktisk workshop.

# Målet
Introducera containers och Docker, samt Vagrant.

Efter workshopen alla ska ha Docker och docker-compose installerat på sin dator.
