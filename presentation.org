#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: toc:nil num:nil
#+AUTHOR:
#+DATE:
#+TITLE: Docker & Vagrant
#+SUBTITLE: Containers and VMs
* A brief history of approaches to application deployment
[[./containers-vm-bare-metal.png]]
** 
In the beginning applications were deployed directly on *bare metal* (directly
on the server). This results in higher costs, wasted resources and expensive
scalability.

During the late 80s and 90s VM-based deployments gained in popularity. They
allow for a better use of the hardware and scale better, but each VM has a full
copy of the OS and libraries.

In the latest years containers are taking over the industry. They run on top of
the OS and can share parts of the filesystem, making them potentially very
lightweight.
** A better way of using VMs
[[https://www.vagrantup.com]]
** Containers in a nutshell
Containers are processes isolated from one another that bundle their own
software, libraries and configuration files; they can communicate with each
other through well-defined channels. Because all of the containers share the
services of a single operating system kernel, they use fewer resources than
virtual machines.
* Docker quickstart
Docker is arguably the most popular container provider today. On linux it runs
~containerd~, a daemon that takes care of spawning the containers. It exposes an
API, by default on ~unix:///var/run/docker.sock~ that other frontends (and
containers) can use to control the daemon.

[[https://docs.docker.com/get-started/]]

A very good book about Docker is ~Docker Deep Dive~ by Nigel Poulton.

The container world is evolving very rapidly, for example the book mentioned
above gets a new edition and updated content every single year.
** Terminology
A ~container~ is a process created starting from an ~image~. The ~image~ is like
a blueprint for creating containers.

Containers get their own isolated root filesystem, but files and directories can
be shared between containers and the host OS via ~volumes~.

Containers live in virtual ~networks~, which can be of different kinds. Examples
are bridged networks, vlans, macvlans, or they can even share the host's network
directly.

They can ~expose~ (or map) ports to the host OS. 
** Getting started
After installing docker, we can run:

~docker run -d -p 80:80 docker/getting-started~

~-d~: runs as a daemon in the background

~-p x:y~: map port ~x~ on the *host* with port ~y~ inside the *container*
* Orchestration
Orchestration is the process of managing several containers together. Starting,
stopping and scaling them without the need to manually manage each container.

Example: to deploy a test environment for an app, you'll need containers for:
the backend, the frontend and the db.

Examples of the most popular orchestrators:
- ~docker-compose~: easy, ideal for dev or smaller deployments (I personally use
  compose even in prod)
- ~docker swarm~: more flexible than compose, better for larger-scale
  environments.
- ~Kubernetes~: very flexible, very powerful, very complex. It's rapidly
  becoming more and more popular.


* "Oh, but it works on my machine..."
...isn't a problem anymore.

We'll use ~docker~ and ~docker-compose~ for the remaning of the course during
workshops and exercises to ensure we're all working on the same environment, to
minimize bugs and problems due to difference in platform, versions, etc.

If for any reason Docker doesn't work well on your OS, I suggest you use Vagrant
to run a Linux VM, and run Docker inside the VM.
* Workshop
** Install docker
[[https://docs.docker.com/get-docker/]]

and run

~docker up -d -p 80:80 docker/getting-started~

then visit [[http://localhost]] with your browser.
** Install Vagrant

[[https://www.vagrantup.com]]

and test spawning an Ubuntu 20.04 VM with:

~vagrant init ubuntu/focal64~

~vagrant up~

~vagrant ssh~
** (optionally) Install and run docker in a Vagrant VM
- Create a new directory, and run ~vagrant init~ in it
- Read through the ~Vagrantfile~ as it describes how to customize it and
  provides useful examples
- Near the end of the file, there's a block starting with ~Enable provisioning
  with a shell script.~. Uncomment and modify that block so that Docker and
  docker-compose are installed when provisioning the VM
** Install docker-compose
[[https://docs.docker.com/compose/install/]]

and make sure it works:

~docker-compose version~


